import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;



public class MySQLSample {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		// ------JDBCコピペ1ここから-----------------------
		        final String DRIVER_NAME = "com.mysql.jdbc.Driver";// MySQLドライバ
		        final String DB_URL = "jdbc:mysql://localhost:3306/";// DBサーバー名
		        final String DB_NAME = "example";// データベース名、使いたいデータベースによって変える
		        final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8";// 文字化け防止
		        final String JDBC_URL = DB_URL + DB_NAME + DB_ENCODE;// 接続DBとURL
		        final String DB_USER = "root";// ユーザーID
		        final String DB_PASS = "root";// パスワード
		        // -------JDBCコピペ1ここまで------------------------

		     // JDBCコピペ2ここから--------------------------------
		             try
		             {
		                     Class.forName(DRIVER_NAME);
		             }catch(
		             ClassNotFoundException e)
		             {
		                     e.printStackTrace();//エラー内容を表示
		             }
		             // JDBCコピペ2ここまで--------------------------------

		        try(Connection conn =
		        		DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)) {//データベースに接続する処理
		        	//selsect文を準備
		        	String sql = "SELECT * FROM employee WHERE id =?";//?のことをプレースホルダという
		        	//sqlをサーバーに送る命令
		        	PreparedStatement pStmt = conn.prepareStatement(sql);
		        	//1つ目の?にEMP002が入るという意味、
		        	String x = "EMP002";
		        	pStmt.setString(1, x);
		        	//select文を実行し結果を取得
		        	ResultSet rs = pStmt.executeQuery();
		        	//結果表に格納されたレコードの内容を表示
		        	//レコード数分処理を繰り返し,次のデータがある間は繰り返し
		        	while(rs.next()){
		        		//フィールドの値を変数に代入
		        		String id = rs.getString("id");
		        		String name = rs.getString("name");
		        		int age = rs.getInt("age");
		        		//結果をコンソールに表示
		        		System.out.println("id:" + id);
		        		System.out.println("名前:" + name);
		        		System.out.println("年齢:" + age);
		        	}

				} catch (Exception e) {
						e.printStackTrace();//エラー内容を表示
				}
	}

}
