package model;


	import java.io.Serializable;
	//健康診断に関する情報を持つJavaBeans//
	public class Health implements Serializable{
		private double heigth, weigth, bmi;
		private String bodyType;

		public void setHeigth(double heigth){
			this.heigth = heigth;
		}
		public double getHeigth(){
			return this.heigth;
		}

		public void setWeigth(double weigth){
			this.weigth = weigth;
		}
		public double getWeigth(){
			return this.weigth;
		}

		public void setBmi(double bmi){
			this.bmi = bmi;
		}
		public double getBmi(){
			return this.bmi;
		}

		public void setBodyType(String bodyType){
			this.bodyType = bodyType;
		}
		public String getBodyType(){
			return this.bodyType;
		}
	}

