package model;

import java.io.Serializable;
//↓Serializableを実装する//
public class Human implements Serializable  {
	private String name;
	private int age;
	//お約束↓中身は空でOK//
	public Human(){}
	//↓追加でコンストラクタを追加してもよい//
	public Human(String name, int age){
		this.name = name;
		this.age = age;
	}
	//↓フィールドの値に対応したゲッター、セッターを作る//
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
	public void setAge(int age){
		this.age = age;
	}
	public int getAge(){
		return this.age;
	}
}
