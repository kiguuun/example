package model;
//健康診断に関する処理を行うモデル//
public class HealthCheackLogic{
	public void execute(Health health){
		//BMIを算出して設定//
		double weigth = health.getWeigth();
		double heigth = health.getHeigth();
		double bmi = weigth / (heigth / 100.0 * heigth / 100.0);
		health.setBmi(bmi);

		//BMI指数から体型を判定して設定//
		String bodyType;
		if(bmi < 18.5){
			bodyType = "やせ型";
		}else if(bmi < 25){
			bodyType = "普通";
		}else{
			bodyType = "肥満";
		}
		health.setBodyType(bodyType);
	}
}
