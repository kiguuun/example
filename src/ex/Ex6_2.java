package ex;

import java.io.IOException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Ex6_2
 */
@WebServlet("/Ex6_2")
public class Ex6_2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int nums = new Random().nextInt(10);
		if(nums % 2 ==1){
			response.sendRedirect("/example/redirected.jsp");
		}else{
			request.getRequestDispatcher("/WEB-INF/jsp/forwarded.jsp").forward(request, response);
		}
	}

}
