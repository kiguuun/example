package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.Employee;

public class EmployeeDao {
	// ------JDBCコピペ1ここから-----------------------メソッドの外に貼る
    final String DRIVER_NAME = "com.mysql.jdbc.Driver";// MySQLドライバ
    final String DB_URL = "jdbc:mysql://localhost:3306/";// DBサーバー名
    final String DB_NAME = "example";// データベース名、使いたいデータベースによって変える
    final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8";// 文字化け防止
    final String JDBC_URL = DB_URL + DB_NAME + DB_ENCODE;// 接続DBとURL
    final String DB_USER = "root";// ユーザーID
    final String DB_PASS = "root";// パスワード
    // -------JDBCコピペ1ここまで------------------------

    //データを全て受け取るメソッドを作る
    public List<Employee> findAll(){
    	List<Employee> empList = new ArrayList<>();
    	// JDBCコピペ2ここから--------------------------------メソッドの中に貼る
        try
        {
                Class.forName(DRIVER_NAME);
        }catch(
        ClassNotFoundException e)
        {
                e.printStackTrace();//エラー内容を表示
        }
        // JDBCコピペ2ここまで--------------------------------

        //データベースへ接続
        try(Connection conn = DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS) ){
			//select文を準備
        	String sql = "SELECT * FROM employee";
        	PreparedStatement pStmt = conn.prepareStatement(sql);

        	//selectを実行し、結果を取得
        	ResultSet rs = pStmt.executeQuery();

        	//結果表に格納されたレコードの内容をemployeeインスタンスに設定しArrayListインスタンスに追加
        	while(rs.next()){
        		String id = rs.getString("id");//レコードの値
        		String name = rs.getString("name");//レコードの値
        		int age = rs.getInt("age");//レコードの値
        		Employee employee = new Employee(id, name, age);//レコードの値を代入
        		empList.add(employee);//employeeを代入
        	}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}



		return empList;
    }
    //idを入力して1件のデータを検索する
    public List<Employee> findById(String x){
    	List<Employee> empList = new ArrayList<>();
    	// JDBCコピペ2ここから--------------------------------メソッドの中に貼る
        try
        {
                Class.forName(DRIVER_NAME);
        }catch(
        ClassNotFoundException e)
        {
                e.printStackTrace();//エラー内容を表示
        }
        // JDBCコピペ2ここまで--------------------------------

        //データベースへ接続
        try(Connection conn = DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS) ){
			//select文を準備
        	String sql = "SELECT * FROM employee WHERE id =?";
        	PreparedStatement pStmt = conn.prepareStatement(sql);
        	//?の内容を入れる
        	pStmt.setString(1, x);

        	//selectを実行し、結果を取得
        	ResultSet rs = pStmt.executeQuery();

        	//結果表に格納されたレコードの内容をemployeeインスタンスに設定しArrayListインスタンスに追加
        	while(rs.next()){
        		String id = rs.getString("id");//レコードの値
        		String name = rs.getString("name");//レコードの値
        		int age = rs.getInt("age");//レコードの値
        		Employee employee = new Employee(id, name, age);//レコードの値を代入
        		empList.add(employee);//employeeを代入
        	}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}



		return empList;
    }
    //データを１件レコードに追加する
    public boolean create(Employee employee){

    	// JDBCコピペ2ここから--------------------------------メソッドの中に貼る
        try
        {
                Class.forName(DRIVER_NAME);
        }catch(
        ClassNotFoundException e)
        {
                e.printStackTrace();//エラー内容を表示
        }
        // JDBCコピペ2ここまで--------------------------------

        //データベースへ接続
        try(Connection conn = DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS) ){
			//INSERT文を準備
        	String sql = "INSERT INTO employee (id,name,age) values (?,?,?)";
        	PreparedStatement pStmt = conn.prepareStatement(sql);
        	//?に値を入れる
        	pStmt.setString(1, employee.getId());
        	pStmt.setString(2, employee.getName());
        	pStmt.setInt(3, employee.getAge());

        	//INSERT文を実行(resultには追加された行数が代入される)
        	int result = pStmt.executeUpdate();
        	if(result !=1){
        		return false;
        	}


		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}

		return true;
    }
}
