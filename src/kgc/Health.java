package kgc;

import java.io.Serializable;

public class Health implements Serializable{
	private double heigth;
	private double weigth;
	private double bmi;
	private String bodyType;

	public Health(){}
	public Health(double heigth, double weigth, double bmi, String bodyType){
		this.bodyType = bodyType;
		this.heigth = heigth;
		this.weigth = weigth;
		this.bmi = bmi;
	}

	public void setBodyType(String bodyType){
		this.bodyType = bodyType;
	}
	public String getBodyType(){
		return this.bodyType;
	}

	public void setHeigth(double heigth){
		this.heigth = heigth;
	}
	public double getHeigth(){
		return this.heigth;
	}

	public void setWeigth(double weigth){
		this.weigth = weigth;
	}
	public double getWeigth(){
		return this.weigth;
	}

	public void setBmi(double bmi){
		this.bmi = bmi;
	}
	public double getBmi(){
		return this.bmi;
	}
}

