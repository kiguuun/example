import java.util.List;

import dao.EmployeeDao;
import model.Employee;

/**検索結果の実行ファイル
 * @author 200217AM
 *
 */
public class SelectEmployeeSample {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		//employeeテーブルの全レコードを取得
		EmployeeDao empDao = new EmployeeDao();
		Employee employee = new Employee("EMP100", "斎藤",45);//追加されるインスタンス
		//追加するメソッドを実行
		empDao.create(employee);
		List<Employee> empList = empDao.findAll();//全件出力したい時はfindAll()メソッドに変える

		//取得したレコード内容を出力
		for(Employee emp : empList){
			System.out.println(emp.getId());
			System.out.println(emp.getName());
			System.out.println(emp.getAge() + "\n");
		}
	}

}
