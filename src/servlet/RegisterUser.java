package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.RegisterUserLogic;
import model.User;

/**
 * Servlet implementation class RegisterUser
 */
//ユーザー登録に関するリクエストを処理するコントローラ//
@WebServlet("/RegisterUser")
public class RegisterUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//①フォワード先//
		String forwardPath = null;
		//サーブレットクラスの動作を決定するactionの値をリクエストパラメータから取得//
		String action = request.getParameter("action");
		//登録の開始をリクエストされたときの処理//
		if(action == null){
			//②フォワード先を設定 一回目はnull//
			forwardPath = "/WEB-INF/jsp/registerForm.jsp";
		}
		//⑫登録確認画面から登録実行をリクエストされたときの処理//
		else if(action.equals("done")){
			//⑬セッションスコープに保存された登録ユーザー情報を取得//
			HttpSession session = request.getSession();
			User registerUser = (User)session.getAttribute("registerUser");
			//⑭登録処理の呼び出し//
			RegisterUserLogic logic = new RegisterUserLogic();
			logic.excute(registerUser);
			//⑮不要となったセッションスコープの削除//
			session.removeAttribute("registerUser");
			//⑯登録後のフォワード先を設定//
			forwardPath = "/WEB-INF/jsp/registerDone.jsp";
		}
		//③,⑰設定されたフォワード先にフォワード//
		RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//⑤リクエストパラメータの取得//
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String pass = request.getParameter("pass");

		//⑥登録するユーザー情報の設定（インスタンス）で保存//
		User registerUser = new User(id, name, pass);
		//⑦セッションスコープで登録ユーザーを保存//
		HttpSession session = request.getSession();
		session.setAttribute("registerUser", registerUser);
		//⑧フォワード//
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registerConfrm.jsp");
		dispatcher.forward(request, response);
	}

}
