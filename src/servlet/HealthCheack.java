package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Health;
import model.HealthCheackLogic;

/**
 * Servlet implementation class HealthCheack
 */
@WebServlet("/HealthCheack")
public class HealthCheack extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//フォワード//
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/healthCheack.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//リクエストパラメータを取得//
		String weigth = request.getParameter("weigth");//体重//
		String heigth = request.getParameter("heigth");//身長//
		//もしエラーチェックするならここに記述//
		//入力値をプロパティに設定//
		Health health = new Health();
		//リクエストパラメータ自体が文字列なのでdouble型に変換//
		health.setHeigth(Double.parseDouble(heigth));
		health.setWeigth(Double.parseDouble(weigth));
		//健康診断を実行し結果を設定//
		HealthCheackLogic healthCheackLogic = new HealthCheackLogic();
		//インスタンスを引数で渡しているのでメソッドで処理した結果が反映されている//
		healthCheackLogic.execute(health);
		//反映されたインスタンスをリクエストスコープに保存//
		request.setAttribute("health", health);
		//フォワード//
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/healthCheackResult.jsp");
		dispatcher.forward(request, response);
	}

}
