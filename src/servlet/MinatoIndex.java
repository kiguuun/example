package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.SiteEV;
import model.SiteEVLogic;

/**
 * Servlet implementation class MinatoIndex
 */
@WebServlet("/MinatoIndex")
public class MinatoIndex extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//アプリケーションスコープに保存されたサイト評価を取得する//
		ServletContext application = this.getServletContext();
		//アプリケーションスコープに保存されているインスタンスを取得//
		SiteEV siteEV = (SiteEV)application.getAttribute("siteEV");

		//サイト評価の初期化（初回リクエスト実行時）//
		if(siteEV == null){
			siteEV = new SiteEV();
		}
		//リクエストパラメータの取得//
		request.setCharacterEncoding("UTF-8");
		String action = request.getParameter("action");

		//サイトの評価処理（初回）リクエスト時は実行しない//
		SiteEVLogic siteEVLogic = new SiteEVLogic();
		if(action !=null && action.equals("like")){
			siteEVLogic.like(siteEV);
		}else if(action != null && action.equals("dislike")){
			siteEVLogic.dislike(siteEV);
		}

		//if文の処理を行った後の結果をアプリケーションスコープにサイト評価を保持//
		application.setAttribute("siteEV", siteEV);

		//フォワード//
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/minatoIndex.jsp");
		dispatcher.forward(request, response);
	}

}
