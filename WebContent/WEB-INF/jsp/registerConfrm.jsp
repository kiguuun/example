<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%//ユーザー登録確認画面を表示するビュー// %>
    <%@ page import = "model.User" %>
    <%//⑨セッションスコープに保存したインスタンスを取得// %>
    <% User registerUser = (User)session.getAttribute("registerUser");%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録確認</title>
</head>
<body>
<p>下記のユーザー情報を登録します。</p>
<!-- ⑩インスタンスの変数の値を表示 -->
<p>ログインID:<%=registerUser.getId() %><br>
名前:<%=registerUser.getName() %><br>
</p>
<a href = "/example/RegisterUser">戻る</a>
<!-- ⑪RegisterUserサーブレットにリクエストパラメータactionの値を送る -->
<a href = "/example/RegisterUser?action=done">登録</a>
</body>
</html>