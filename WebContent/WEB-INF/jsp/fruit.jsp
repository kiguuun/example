<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import = "ex.Fruit" %>
    <% Fruit fruit = (Fruit)session.getAttribute("fruit"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>7章練習問題</title>
</head>
<body>
<p>
<%=fruit.getName() %>の値段は<%=fruit.getPrice() %>円です。
</p>
<%session.invalidate(); %>
</body>
</html>