<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <!-- varは変数名、valueは変数に入るもの -->
    <c:set var = "name" value="ミナト"></c:set>
    <c:set var="age" value="23"></c:set>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<p><c:out value="${name }"></c:out></p>
<p><c:out value="${age }"></c:out></p>

<!-- c:if8は分岐できない -->
<c:if test="${age>=20 }">
<p>あなたは成人です。</p>
</c:if>

<!-- 分岐のさせ方↓<when>タグは複数記述できる -->
<c:choose>
<c:when test="${age>=50 }">
あなたは成人です。
</c:when>
<c:otherwise>
あなたは未成年です。
</c:otherwise>
</c:choose>

<!-- 繰り返し -->
<c:forEach var="i" begin="0" end="9" step="1">
<c:out value="${i }"></c:out>
</c:forEach>

</body>
</html>